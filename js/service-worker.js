/*jshint esversion: 11 */

self.addEventListener("install", async (e) => {
	await e.waitUntil((async () => {
		const CONFIG = await (await fetch("/static/config.json")).json();
		caches.open("cache-" + CONFIG.version).then((cache) => {
			cache.add("/");
			cache.add("/manifest.json");
			cache.add("/static/config.json");
			cache.addAll(CONFIG.styles);
			cache.addAll(CONFIG.images);
			cache.addAll(CONFIG.scripts);
			cache.addAll(Object.values(CONFIG.templates).map(i => "/static/templates/" + i));
			cache.addAll(Object.values(CONFIG.menus).map(i => "/static/menus/" + i));
			cache.addAll(Object.values(CONFIG.modals).map(i => "/static/modals/" + i));
			self.skipWaiting();
		});
	})());
});

self.addEventListener("activate", async (e) => {});

self.addEventListener("fetch", (e) => {
	if(!e.request.url.startsWith(location.origin)) {
		e.respondWith((async () => {
			return await fetch(e.request);
		})());
	}
	else if (e.request.url.endsWith("/static/config.json"))
		e.respondWith((async () => {
			const response = await fetch("/static/config.json");
			if (response.status === 200)
				return response;
			else {
				console.warn(`Can't get config.json.\nUsing cached version.`);
				return caches.match(new Request("/static/config.json"));
			}
		})());
	else if (e.request.url.split("/").at(-1).includes("."))
		e.respondWith((async () => {
			const match = await caches.match(e.request);
			if(match)
				return match;
			else return await fetch(e.request.url);
		})());
	else e.respondWith(caches.match(new Request("/")));
});
