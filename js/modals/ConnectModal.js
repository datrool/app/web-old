/*jshint esversion: 11 */

import {BaseModal} from "/js/modals/BaseModal.js";
import {Connect} from "/js/modules/Connect.js";

export class ConnectModal extends BaseModal {
	constructor() {
		super("connect", "API connection", "small");
		let input = document.querySelector("#modal input#connect-host");
		let button = document.querySelector("#modal button#connect-send");
		input.value = localStorage.getItem("api_endpoint") || "";

		button.addEventListener("click", () => {
			const pattern = /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi;
			const host = input.value.endsWith("/") ? input.value.slice(0, -1) : input.value;
			if(host.match(pattern) || host.match(/http:\/\/localhost:?./gi)) {
				const endpoint = window.endpoint.get(host);
				endpoint.addEventListener("load", (r) => Connect.version_check(r), "GET");
				fetch(host).catch(e => app.notifier.error("<b>Connection error</b><br />Cannot connect to API server."));
			}
		});
	}
}
