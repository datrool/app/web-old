/*jshint esversion: 11 */

export class WebNotifier extends HTMLElement {
	get duration() {
		let duration = this.getAttribute("default-duration");
		return duration && parseInt(duration) == duration ? Math.abs(parseInt(duration)) : 5000;
	}

	set duration(v) {
		this.setAttribute("duration", v || "");
	}

	get animationDuration() {
		let al = this.getAttribute("animation-duration");
		return al && parseInt(al) == al ? Math.abs(parseInt(al)) : 500;
	}

	set animationDuration(v) {
		this.setAttribute("animation-duration", v || 0);
	}

	push(message, type="info", duration=null) {
		if(!message)
			return;
		const notification = document.createElement("web-notification");
		if(type)
			notification.classList.add(type);
		notification.show(message);
		this.appendChild(notification);
		duration = typeof(duration) == "number" ? duration : this.duration;
		const remove = () => setTimeout(() => {notification.remove(this.animationDuration);}, duration);
		let timeout = remove();
		notification.addEventListener("mouseover", () => { clearTimeout(timeout); });
		notification.addEventListener("mouseout", () => { timeout = remove(); });
	}

	info(message) {
		this.push(message);
	}
	warn(message) {
		this.push(message, "warning");
	}
	error(message) {
		this.push(message, "error");
	}
}

export class WebNotification extends HTMLElement {
	constructor() {
		super();
		let _this = this;
		this.text = document.createElement("span");
		this.close = document.createElement("button");
		this.close.addEventListener("click", function() { _this.remove(); });
	}

	connectedCallback() {
		this.appendChild(this.text);
		this.appendChild(this.close);
	}

	show(text) {
		this.text.innerHTML = text;
		this.classList.add("show");
	}

	hide() {
		this.classList.remove("show");
	}

	remove(animationDuration) {
		this.hide();
		setTimeout(() => {
			if(this.parentNode)
				this.parentNode.removeChild(this);
		}, animationDuration || 1);
	}
}

window.customElements.define('web-notifier', WebNotifier);
window.customElements.define('web-notification', WebNotification);
