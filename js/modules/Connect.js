/*jshint esversion: 11 */
import {ConnectModal} from "/js/modals/ConnectModal.js";

export class Connect {
	static version_check(r) {
		console.log("here?", r);
		if(!r.status)
			return window.app.error("<b>Connection error</b><br />Cannot connect to API server.");

		const check = (version) => {
			if(version === "API_VERSION")
				return true;
			const version_parts = version.replace("v").split(".");
			const required_parts = "API_VERSION".replace("v").split(".");
			for(let i=0; i < version_parts.length; i++)
				if(version_parts[i] < required_parts[i])
					return false;
			return true;
		};

		if(!check(r.version)) {
			window.app.error(`<b>Connection error</b><br />Old API version (${r.version}). <br />Update to __API_VERSION__ or newer.`);
			return;
		}
		localStorage.setItem("api_endpoint", r.host);
		location.reload();
	}

	static router() {
		const connect = window.route.get("/connect");
		connect.addEventListener("load", () => { new ConnectModal(); });
	}
}
