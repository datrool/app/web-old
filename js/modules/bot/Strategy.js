/*jshint esversion: 11 */
import {StrategyModal} from "/js/modals/StrategyModal.js";
import {ConfirmModal} from "/js/modals/BaseModal.js";

export class Strategy {
	static list(r) {
		document.querySelector("#strategy-add").addEventListener("click", () => {
			window.history.pushState(null, "", "/strategies/create");
		});
		const strategy_list = document.querySelector("table#strategy-list tbody");

		for(let i in r.strategies) {

			let version = [0,0,0];
			for(let v in r.strategies[i].versions) {
				const split = r.strategies[i].versions[v].split(".");
				if(parseInt(split[0]) >= version[0] && parseInt(split[1]) >= version[1] && parseInt(split[2]) >= version[2])
					version = [parseInt(split[0]),parseInt(split[1]),parseInt(split[2])];
			}

			const name = document.createElement("td");
			const version_td = document.createElement("td");
			const owner = document.createElement("td");
			const is_public = document.createElement("td");
			is_public.classList.add("icon");
			const activity = document.createElement("td");

			name.innerHTML = r.strategies[i].name;
			version_td.innerHTML = version.join(".");
			owner.innerHTML = r.strategies[i].owner;
			is_public.innerHTML = r.strategies[i].public ? "&#xE836;" : "&#xE837;";
			is_public.style = "font-size: 20px; text-align: left";

			const editBtn = document.createElement("button");
			editBtn.classList.add("edit");
			editBtn.title = "Update";
			const deleteBtn = document.createElement("button");
			deleteBtn.classList.add("delete");
			deleteBtn.title = "Delete";
			activity.appendChild(editBtn);
			activity.appendChild(deleteBtn);
			activity.classList.add("activity");

			const tr = document.createElement("tr");
			tr.dataset.id = r.strategies[i].id;
			tr.dataset.name = r.strategies[i].name;
			tr.dataset.version = version.join(".");
			tr.appendChild(name);
			tr.appendChild(version_td);
			tr.appendChild(owner);
			tr.appendChild(is_public);
			tr.appendChild(activity);
			strategy_list.appendChild(tr);

			editBtn.addEventListener("click", () => {
				window.history.pushState(null, "", `/strategies/update/${tr.dataset.id}/${tr.dataset.version}`);
			});
			deleteBtn.addEventListener("click", () => {
				window.history.pushState({
					title: "Delete strategy",
					name: tr.dataset.name
				}, "", "/strategies/delete/" + tr.dataset.id);
			});
		}
	}

	static add() {
		new StrategyModal("create");
	}

	static edit(response) {
		new StrategyModal("update", response);
	}

	static delete(response) {
		const parts = location.pathname.split("/");
		const id = location.pathname.endsWith("/") ? parts.at(-2) : parts.at(-1);
		fetch(window.app.endpoint + "/strategy/delete/" + id, {method: "DELETE"});
	}

	static router() {
		const list = window.route.get("/strategies");
		const add = window.route.get("/strategies/create");
		const update = window.route.get(/^\/strategies\/update\/\d+\/\d+\.\d+\.\d+\/?$/);
		const remove = window.route.get(/^\/strategies\/delete\/\d+\/?$/);

		list.addEventListener("load", () => { fetch(window.app.endpoint + "/strategies"); });
		add.addEventListener("load", () => { Strategy.add(); });
		update.addEventListener("load", () => {
			const parts = location.pathname.split("/");
			const version = location.pathname.endsWith("/") ? parts.at(-2) : parts.at(-1);
			const id = location.pathname.endsWith("/") ? parts.at(-3) : parts.at(-2);
			fetch(window.app.endpoint + `/strategy/${id}/${version}`).then(r => {
				r.json().then(d => this.edit(d));
			});
		});
		remove.addEventListener("load", (state) => {
			const message = `Confirm deletion of strategy \"${state.name}\"`;
			new ConfirmModal(message, "Confirm deletion", Strategy.delete, null, "red");
		});
	}
	static endpoint() {
		const strategies = window.endpoint.get(window.app.endpoint + "/strategies");
		strategies.addEventListener("load", (r) => Strategy.list(r), "GET");
	}
}
