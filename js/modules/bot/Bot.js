/*jshint esversion: 11 */
import {BotModal} from "/js/modals/BotModal.js";
import {ConfirmModal} from "/js/modals/BaseModal.js";

export class Bot {
	static list(r) {
		document.querySelector("#bot-add").addEventListener("click", () => {
			window.history.pushState(null, "", "/bots/create");
		});
		const types = ["Trader", "Notifier", "Tester"];
		const bot_list = document.querySelector("table#bot-list tbody");
		for(let i in r.bots) {
			const state = document.createElement("td");
			const name = document.createElement("td");
			const tags = document.createElement("td");
			const host = document.createElement("td");
			const port = document.createElement("td");
			const activity = document.createElement("td");

			const stateImg = new Image();
			if(r.bots[i].state === 0) {
				stateImg.src = "/img/running.png";
				state.title = "Running";
			}
			if(r.bots[i].state === 1) {
				stateImg.src = "/img/stopped.png";
				state.title = "Stopped";
			}
			if(r.bots[i].state === 2) {
				stateImg.src = "/img/disconnected.png";
				state.title = "Disconnected";
			}
			state.appendChild(stateImg);

			const type = document.createElement("span");
			type.classList.add("tag");
			type.innerHTML = types[r.bots[i].type];
			tags.appendChild(type);

			name.innerHTML = r.bots[i].name;
			host.innerHTML = r.bots[i].host;
			port.innerHTML = r.bots[i].port;

			const editBtn = document.createElement("button");
			editBtn.classList.add("edit");
			editBtn.title = "Edit";
			const deleteBtn = document.createElement("button");
			deleteBtn.classList.add("delete");
			deleteBtn.title = "Delete";
			activity.appendChild(editBtn);
			activity.appendChild(deleteBtn);
			activity.classList.add("activity");

			const tr = document.createElement("tr");
			tr.dataset.id = r.bots[i].id;
			tr.dataset.name = r.bots[i].name;
			tr.appendChild(state);
			tr.appendChild(name);
			tr.appendChild(tags);
			tr.appendChild(host);
			tr.appendChild(port);
			tr.appendChild(activity);
			bot_list.appendChild(tr);

			editBtn.addEventListener("click", () => {
				window.history.pushState(null, "", "/bots/edit/" + tr.dataset.id);
			});
			deleteBtn.addEventListener("click", () => {
				window.history.pushState({
					title: "Delete service",
					name: tr.dataset.name
				}, "", "/bots/delete/" + tr.dataset.id);
			});
		}
	}

	static add() {
		new BotModal("add");
	}

	static edit(response) {
		new BotModal("edit", response);
	}

	static delete(response) {
		const parts = location.pathname.split("/");
		const id = location.pathname.endsWith("/") ? parts.at(-2) : parts.at(-1);
		fetch(window.app.endpoint + "/bot/delete/" + id, {method: "DELETE"});
	}

	static router() {
		const list = window.route.get("/bots");
		const add = window.route.get("/bots/create");
		const edit = window.route.get(/^\/bots\/edit\/\d+\/?$/i);
		const remove = window.route.get(/^\/bots\/delete\/\d+\/?$/i);
		list.addEventListener("load", () => { fetch(window.app.endpoint + "/bots"); });
		add.addEventListener("load", () => { Bot.add(); });
		edit.addEventListener("load", () => {
			const parts = location.pathname.split("/");
			const id = location.pathname.endsWith("/") ? parts.at(-2) : parts.at(-1);
			fetch(window.app.endpoint + "/bot/" + id);
		});
		remove.addEventListener("load", (state) => {
			const message = `Confirm deletion of service \"${state.name}\"`;
			new ConfirmModal(message, "Confirm deletion", Bot.delete, null, "red");
		});
	}

	static endpoint() {
		const bots = window.endpoint.get(window.app.endpoint + "/bots");
		const bot = window.endpoint.get(new RegExp("^" + window.app.endpoint + "/bot/\\d+/?$"));
		// const add = window.endpoint.get(window.app.endpoint + "/bot/create");
		// const edit = window.endpoint.get(window.app.endpoint + "/bot/update");
		const remove =window.endpoint.get(new RegExp("^" + window.app.endpoint + "/bot/delete/\\d+/?$"));
		bots.addEventListener("load", (r) => Bot.list(r), "GET");
		bot.addEventListener("load", (r) => {
			Bot.edit(r);
		}, "GET");
		// add.addEventListener("load", (r) => {}, "POST");
		// edit.addEventListener("load", (r) => {}, "PUT");
		remove.addEventListener("load", response => {
			if(!response.status)
				app.notiifer.error("<b>Cannot remove robot.");
		}, "DELETE");
	}
}
