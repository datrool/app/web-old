/*jshint esversion: 11 */
import {IndicatorModal} from "/js/modals/IndicatorModal.js";
import {ConfirmModal} from "/js/modals/BaseModal.js";

export class Indicator {
	static list(r) {
		document.querySelector("#indicator-add").addEventListener("click", () => {
			window.history.pushState(null, "", "/indicators/create");
		});
		const indicator_list = document.querySelector("table#indicator-list tbody");
		for(let i in r.indicators) {

			let version = [0,0,0];
			for(let v in r.indicators[i].versions) {
				const split = r.indicators[i].versions[v].split(".");
				if(parseInt(split[0]) >= version[0] && parseInt(split[1]) >= version[1] && parseInt(split[2]) >= version[2])
					version = [parseInt(split[0]),parseInt(split[1]),parseInt(split[2])];
			}

			const name = document.createElement("td");
			const version_td = document.createElement("td");
			const owner = document.createElement("td");
			const is_public = document.createElement("td");
			is_public.classList.add("icon");
			const activity = document.createElement("td");

			name.innerHTML = r.indicators[i].name;
			version_td.innerHTML = version.join(".");
			owner.innerHTML = r.indicators[i].owner;
			is_public.innerHTML = r.indicators[i].public ? "&#xE836;" : "&#xE837;";
			is_public.style = "font-size: 20px; text-align: left";

			const editBtn = document.createElement("button");
			editBtn.classList.add("edit");
			editBtn.title = "Update";
			const deleteBtn = document.createElement("button");
			deleteBtn.classList.add("delete");
			deleteBtn.title = "Delete";
			activity.appendChild(editBtn);
			activity.appendChild(deleteBtn);
			activity.classList.add("activity");

			const tr = document.createElement("tr");
			tr.dataset.id = r.indicators[i].id;
			tr.dataset.name = r.indicators[i].name;
			tr.dataset.version = version.join(".");
			tr.appendChild(name);
			tr.appendChild(version_td);
			tr.appendChild(owner);
			tr.appendChild(is_public);
			tr.appendChild(activity);
			indicator_list.appendChild(tr);

			editBtn.addEventListener("click", () => {
				window.history.pushState(null, "", `/indicators/update/${tr.dataset.id}/${tr.dataset.version}`);
			});
			deleteBtn.addEventListener("click", () => {
				window.history.pushState({
					title: "Delete indicator",
					name: tr.dataset.name
				}, "", "/indicators/delete/" + tr.dataset.id);
			});
		}
	}

	static add() {
		new IndicatorModal("create");
	}

	static edit(response) {
		new IndicatorModal("update", response);
	}

	static delete(response) {
		const parts = location.pathname.split("/");
		const id = location.pathname.endsWith("/") ? parts.at(-2) : parts.at(-1);
		fetch(window.app.endpoint + "/indicator/delete/" + id, {method: "DELETE"});
	}

	static router() {
		const list = window.route.get("/indicators");
		const add = window.route.get("/indicators/create");
		const update = window.route.get(/^\/indicators\/update\/\d+\/\d+\.\d+\.\d+\/?$/);
		const remove = window.route.get(/^\/indicators\/delete\/\d+\/?$/);

		list.addEventListener("load", () => { fetch(window.app.endpoint + "/indicators"); });
		add.addEventListener("load", () => { Indicator.add(); });
		update.addEventListener("load", () => {
			const parts = location.pathname.split("/");
			const version = location.pathname.endsWith("/") ? parts.at(-2) : parts.at(-1);
			const id = location.pathname.endsWith("/") ? parts.at(-3) : parts.at(-2);
			fetch(window.app.endpoint + `/indicator/${id}/${version}`).then(r => {
				r.json().then(d => this.edit(d));
			});
		});
		remove.addEventListener("load", (state) => {
			const message = `Confirm deletion of indicator \"${state.name}\"`;
			new ConfirmModal(message, "Confirm deletion", Indicator.delete, null, "red");
		});
	}
	static endpoint() {
		const indicators = window.endpoint.get(window.app.endpoint + "/indicators");
		indicators.addEventListener("load", (r) => Indicator.list(r), "GET");
	}
}
