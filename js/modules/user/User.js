/*jshint esversion: 11 */
import {LoginModal} from "/js/modals/LoginModal.js";

export class User {
	static login(response) {
		if (response.status) {
			const modal = new LoginModal();
			modal.hide();
			setTimeout(() => {
				let route = {state: {title: "Portfolio"}, url: "/portfolio"};
				window.app.auth(response.id, response.key, response.device);
				try{
					route = JSON.parse(localStorage.getItem("route")) || route;
				} catch(e) {}
				history.pushState(route.state, route.title, route.url);
			}, 500);
		}
		else window.app.notifier.error("<b>Auth error</b><br />" + response.message);
	}

	static logout() {
		localStorage.removeItem("auth");
		window.app.unauth();
		location.reload();
	}


	static router() {
		const login = window.route.get("/login");
		const logout = window.route.get("/logout");
		login.addEventListener("load", () => { new LoginModal(); });
		logout.addEventListener("load", () => { User.logout(); });
	}

	static endpoint() {
		const endpoint = window.endpoint.get(window.app.endpoint + "/login");
		endpoint.addEventListener("load", (r) => this.login(r), "POST");
	}
}
