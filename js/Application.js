/*jshint esversion: 11 */
import {Commands} from "./modules/bot/Commands.js";
import {RouteList, EndpointList} from "./Linker.js";

class Application {
	#endpoint = localStorage.getItem("api_endpoint");
	#auth = JSON.parse(localStorage.getItem("auth") || "") || {};
	#socket;
	#config;
	#counter = 0;
	#menus = {};
	#modals = {};
	#templates = {};
	#main;
	#menu;
	#loader;
	#notifier;

	constructor() {
		const _ = (_) => {
			return (state, u, url="", skip=false) => {
				if(!/^\/(connect|login|logout)\/?$/i.test(url)) {
					const ref = JSON.parse(localStorage.getItem("route") || "{}");
					localStorage.setItem("route", JSON.stringify({
						state: state,
						url: url,
						referer: {
							state: ref.url !== url ? ref.state : ref.referer.state,
							url: ref.url !== url ? ref.url : ref.referer.url
						}
					}));
				}
				if(!this.#endpoint) {
						state=null;
						url = "/connect";
						skip = true;
				}
				else if(this.#socket && (!this.#auth.id || !this.#auth.key || !this.#auth.device)) {
						state=null;
						url = "/login";
						skip = true;
				}
				else if((this.#socket && (/^\/connect\/?$/i).test(url)) || (this.#auth.id && this.#auth.key && this.#auth.device && (/^\/login\/?$/i).test(url)))
					return;
				document.title = state?.title ? `${state.title} - CoinPaca` : "CoinPaca";
				this.#render(url, state, skip);
				return _.call(history, state, u, url, skip);
			};
		};
		history.pushState = _(history.pushState);
		history.replaceState = _(history.replaceState);
		addEventListener("popstate", e => {
			document.title = e.state?.title ? `${e.state.title} - CoinPaca` : "CoinPaca";
		});
		const _fetch = fetch;
		fetch = (req, opt={}, skip=false) => {
			if(req.includes(this.#endpoint) && this.#auth.id) {
				opt.headers = opt.headers || {};
				opt.headers["token_ik"] = this.#auth.id;
				opt.headers["token"] = this.#auth.key;
				opt.headers["token_device"] = this.#auth.device;
				if (this.teamId)
					opt.headers["team_id"] = this.teamId;
			}
			return _fetch(req, opt, skip);
		};
		let a = 3;
		this.#main = document.querySelector("body > main");
		this.#menu = document.querySelector("body > nav.submenu");
		this.#initialize();
	}

	async #initialize() {
		try { this.#config = await fetch("/static/config.json").then(r => r.json()); }
		catch(e) {
			throw new Error(`Invalid configuration file: ${e.message}`);
		}
		if(this.#config.version !== localStorage.getItem("version"))
			this.#update();
		else {
			if(this.#endpoint) {
				for(let i in EndpointList)
					EndpointList[i].endpoint();
			}
			this.#load("templates", this.#templates);
			this.#load("menus", this.#menus);
			this.#load("modals", this.#modals);
			this.#loader = document.querySelector("web-loader");
			this.#notifier = document.querySelector("web-notifier");
		}
	}

	#update() {
		if(window.debug || !navigator.serviceWorker) {
			localStorage.setItem("version", this.#config.version);
			location.reload();
			return;
		}
		navigator.serviceWorker.getRegistrations().then(e => { e.forEach(e => { e.unregister();}); });
		navigator.serviceWorker.register('/service-worker.js').then((r) => {
			let wait = (r) => {
				if(r.waiting || r.active) {
					localStorage.setItem("version", this.#config.version);
					location.reload();
				}
				else setTimeout(() => { wait(r); }, 100);
			};
			wait(r);
		});
	}

	#load(type, target) {
		for(let key in this.#config[type]) {
			fetch(`/static/${type}/${this.#config[type][key]}`)
			.then(resp => { return resp.text(); }).then(data => {
				target[key] = data;
				if(Object.keys(this.#config[type]).length === Object.keys(target).length && ++this.#counter === 3) {
					for(let i in RouteList)
						RouteList[i].router();
					const route = JSON.parse(localStorage.getItem("route")) || {state: {title: "Portfolio"} , url: "/portfolio"};
					history.replaceState(route.state, "", route.url);
					if(this.#endpoint)
						this.connect(this.#endpoint);
				}
			});
		}
	}

	connect(endpoint) {
		const protocol = location.protocol === "http:" ? "ws:" : "wss:";
		this.#socket = new WebSocket(endpoint.replace(location.protocol, protocol) + "/socket");
		this.#socket.addEventListener("open", () => {
			this.#endpoint = endpoint || "";
			if(this.#auth.id && this.#auth.key && this.#auth.device)
				this.command("auth", this.#auth);
			else if(!/^\/login\/?$/i.test(location.pathname)) {
				history.pushState(null, "", "/login");
			}
		});
		this.#socket.addEventListener("close", () => {
			console.warn("Socket: closed");
			this.#socket = null;
		});
		this.#socket.addEventListener("error", (e) => {
			console.error("Socket:", e);
			this.#socket = null;
			if(!/^\/connect\/?$/i.test(location.pathname))
				history.pushState(null, "", "/connect");
		});
		this.#socket.addEventListener("message", (e) => {
			try { Commands.responseProcessor(JSON.parse(e.data)); }
			catch(e) {
				if(e instanceof SyntaxError)
					console.error("Invalid response", e);
				else throw e;
			}
		});
	}

	command(action, data) {;
		if(this.#socket)
			this.#socket.send(JSON.stringify({"action": action, "data": data}));
		else {
			this.connect(this.#endpoint);
			this.#socket.addEventListener("open", () => {
				setTimeout(() => { this.command(action, data) }, 500);
			});
		}
	}

	#render(template=location.pathname, state=null, skip=false) {
		const content = this.#templates[template.replace(/\/$/, "")];
		if(content && (!skip || !this.#main.innerHTML.length))
			this.#main.innerHTML = content;
		else if(!this.#main.innerHTML.length && template.length > 1 && !skip) {
			history.replaceState(null, "", template.replace(/[^\/]+\/?$/, ""))
			history.replaceState(state, "", template, true);
		}
		const menu = template.split("/")[1];
		if(this.#menus[menu]) {
			this.#menu.dataset.template = template;
			this.#menu.innerHTML = this.#menus[menu];
			this.#menu.removeAttribute("hidden");
		}
		else this.#menu.setAttribute("hidden", '');
	}

	auth(id, key, device) {
		this.#auth = {id: id, key: key, device: device};
		this.command("auth", this.#auth);
		localStorage.setItem("auth", JSON.stringify(this.#auth));
	}

	unauth() {
		this.#auth = {};
	}

	get endpoint() { return this.#endpoint; }
	get modals() { return Object.deepCopy(this.#modals); }
	get version() { return this.#config.version; }
	get loader() { return this.#loader; }
	get notifier() { return this.#notifier; }
}

Object.defineProperty(window, "app", {
	value: new Application(),
	enumerable: false,
	configurable: false,
	writable: false
});
